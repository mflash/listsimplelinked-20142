package pucrs.alpro2;

public class App {
	
	public static void main(String[] args) {
		ListSimpleLinkedEx<Integer> lista = new ListSimpleLinkedEx<>();
		lista.add(1);
		lista.add(3);
		lista.add(5);
		lista.add(7);
		lista.add(9);
		
		lista.add(1,2); // posição 1, inserir 2
		lista.add(3,4); // posição 3, inserir 4
		
		System.out.println("Tamanho: "+lista.size());
		//for(int pos=0; pos<lista.size(); pos++)
		//	System.out.println(lista.get(pos));
		
		System.out.println(lista);
		
		System.out.println("Removendo do início");
		lista.remove(0);
		
		System.out.println(lista);
		
		System.out.println("Removendo do meio - index=4");
		lista.remove(4);
		System.out.println(lista);
		
		System.out.println("Removendo do fim - index=4 (de novo)");
		lista.remove(4);
		System.out.println(lista);
		
		System.out.println("Inserindo no início com addFirst");
		lista.addFirst(1);
		System.out.println(lista);
		
		System.out.println("Removendo do início com removeFirst");
		lista.removeFirst();
		System.out.println(lista);
		
		System.out.println("Removendo do fim com removeLast");
		lista.removeLast();
		System.out.println(lista);
		
		System.out.println("Clone da lista original:");
		ListSimpleLinkedEx<Integer> lista2 = (ListSimpleLinkedEx<Integer>) lista.clone();
		System.out.println(lista2);
		
		System.out.println("Lista original invertida (versão 1)");
		lista2.inverte1();
		System.out.println(lista2);
		System.out.println("Lista original invertida de novo (versão 2)");
		lista2.inverte2();
		System.out.println(lista2);
		
		// Testando os tempos dos dois algoritmos de inversão
		// (surpresa: o 2o. é MUITO MAIS EFICIENTE, apesar de ter mais código)
		lista2.clear();
		for(int pos=0; pos<1000000; pos++)
			lista2.add(pos);
		long tempoIni = System.currentTimeMillis();
		lista2.inverte1();
		long tempoFim = System.currentTimeMillis();
		System.out.println("Tempo da inversão (versão 1): "+ (tempoFim-tempoIni));
		
		tempoIni = System.currentTimeMillis();
		lista2.inverte2();
		tempoFim = System.currentTimeMillis();
		System.out.println("Tempo da inversão (versão 2): "+ (tempoFim-tempoIni));
	}
}
