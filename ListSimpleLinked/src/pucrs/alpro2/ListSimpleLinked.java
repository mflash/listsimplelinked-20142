package pucrs.alpro2;

public class ListSimpleLinked<E> implements ListTAD<E> {

	protected Node<E> head;
	protected Node<E> tail;
	protected int count;

	protected class Node<E> {
		public E element;
		public Node<E> next;

		public Node(E elem) {
			element = elem;
			next = null;
		}
	}

	public ListSimpleLinked() {
		clear();
	}

	@Override
	public void add(E e) {
		Node<E> n = new Node<>(e);
		if (count > 0) // if (tail != null)
			tail.next = n;
		else
			head = n;
		count++;
		tail = n;
	}

	@Override
	public void add(int index, E element) {
		if (index < 0 || index >= count)
			throw new IndexOutOfBoundsException("Posição inválida!");
		Node<E> n = new Node<>(element);

		if (index == 0) // inserir ANTES do primeiro => caso especial
		{
			n.next = head;
			head = n;
		} else {
			Node<E> ant = head;
			for (int pos = 0; pos < index - 1; pos++)
				// pára um elemento antes da desejada
				ant = ant.next;

			Node<E> target = ant.next; // pega o seguinte (pos. desejada)
			ant.next = n; // aponta do anterior para o novo
			n.next = target; // aponta do novo para o seguinte
		}
		count++;
	}

	@Override
	public E get(int index) {
		if(index < 0 || index >= count)
			throw new IndexOutOfBoundsException("Posição inválida!");
		Node<E> target = head;
		for(int pos=0; pos<index; pos++)
			target = target.next;
		return target.element;
	}

	@Override
	public int indexOf(E e) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void set(int index, E element) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean remove(E e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E remove(int index) {
		if(index < 0 || index >= count)
			throw new IndexOutOfBoundsException("Posição inválida!");
		if(index == 0) { // primeiro?
			E elem = head.element;
			head = head.next;
			count--;
			if(head == null) // último?
				tail = null;
			return elem;			
		}
		// Não é o primeiro, caminha até pos. anterior
		Node<E> prev = head;
		for(int pos=0; pos<index-1; pos++)
			prev = prev.next;
		
		Node<E> aux = prev.next; // aux é quem desejo remover
		E elem = aux.element;
		
		// Aponta do prev para o seguinte ao aux
		prev.next = aux.next;
		count--;
		// Último elemento da lista? tail deve apontar para anterior
		if(prev.next == null)
			tail = prev;
		
		return elem;
	}

	@Override
	public boolean isEmpty() {
		return head == null;
	}

	@Override
	public int size() {
		return count;
	}

	@Override
	public boolean contains(E e) {
		Node<E> aux = head;
		boolean achou = false;
		while(aux != null && !achou) {
			if(aux.element.equals(e))
				achou = true;
			aux = aux.next;
		}
		return achou;
	}

	@Override
	public void clear() {
		head = null;
		tail = null;
		count = 0;		
	}
	
	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder();
		Node<E> atual = head;
		while(atual != null) {
			aux.append(atual.element);
			aux.append(" ");
			atual = atual.next;
		}
		return aux.toString();	
	}
}
