package pucrs.alpro2;

public interface ListTADEx<E> extends ListTAD<E> {

	void addFirst(E e);
	E removeFirst();
	E removeLast();
	void inverte1();
	void inverte2();
}
