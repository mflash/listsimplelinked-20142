package pucrs.alpro2;

import java.util.NoSuchElementException;

import pucrs.alpro2.ListSimpleLinked.Node;

public class ListSimpleLinkedEx<E> extends ListSimpleLinked<E>
  implements ListTADEx<E> {

	@Override
	public void addFirst(E e) {
		Node<E> novo = new Node<>(e);
		novo.next = head;
		head = novo;
		if(count == 0) // primeiro elemento a ser inserido?
			tail = head;
		count++;
	}

	@Override
	public E removeFirst() {
		if(count == 0)
			throw new NoSuchElementException("Lista vazia!");
		E aux = head.element;
		head = head.next;
		count--;
		if(count == 0) // lista ficou vazia?
			tail = null;
		return aux;
	}

	@Override
	public E removeLast() {
		if(count == 0)
			throw new NoSuchElementException("Lista vazia!");
		if(count == 1)
			return removeFirst();
		Node<E> aux = head;
		for(int pos=0; pos<count-2; pos++)
			aux = aux.next;
		E elem = aux.next.element;
		aux.next = null;
		tail = aux;
		count--;
		return elem;
	}
	
	@Override
	public Object clone()
	{
		ListSimpleLinkedEx<E> nova = new ListSimpleLinkedEx<>();
		Node<E> atual = head;
		while(atual != null) {
			nova.add(atual.element);
			atual = atual.next;
		}
		return nova;
	}

	@Override
	public void inverte1() {
		ListSimpleLinkedEx<E> nova = new ListSimpleLinkedEx<>();
		Node<E> atual = head;
		while(atual != null) {
			nova.addFirst(atual.element);
			atual = atual.next;
		}
		head = nova.head;
		tail = nova.tail;		
	}

	@Override
	public void inverte2() {
		Object aux[] = new Object[count];
		Node<E> atual = head;
		int pos = 0;
		while(atual != null) {
			aux[pos] = atual;
			pos++;
			atual = atual.next;
		}
		// Troca
		for(pos=0; pos<count/2; pos++) {
			Node<E> n1 = (Node<E>) aux[pos];
			Node<E> n2 = (Node<E>) aux[count-1-pos];
			E temp = n1.element;
			n1.element = n2.element;
			n2.element = temp;
		}		
	}
}
